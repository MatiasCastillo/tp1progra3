package control;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class Calculadora {
	
	private char operacion;
	private String numero;
	private static boolean flagResultado;
	private static boolean flagGuardarMemoria;
	private static boolean flagUsarMemoria;
	private static boolean flagOperacion;
	private static boolean flagAccesibilidad;
	private static String[] numerosEnMemoria;
	
	
	public static boolean isFlagAccesibilidad() {
		return flagAccesibilidad;
	}

	public static void setFlagAccesibilidad(boolean flagAccesibilidad) {
		
		Calculadora.flagAccesibilidad = flagAccesibilidad;
		
	}

	public Calculadora() {
		numero="0";
		flagResultado=false;
		flagGuardarMemoria=false;
		flagUsarMemoria=false;
		numerosEnMemoria=new String[10];
		flagAccesibilidad = false;
		flagOperacion = false;
		
	}

	public char getOp() {
		return operacion;
	}

	public void setOp(char op) {
		this.operacion = op;
	}

	public boolean isFlagOperacion() {
		return flagOperacion;
	}

	public void setFlagOperacion(boolean flagOperacion) {
		Calculadora.flagOperacion = flagOperacion;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public boolean isFlagResultado() {
		return flagResultado;
	}

	public void setFlagResultado(boolean flagResultado) {
		Calculadora.flagResultado = flagResultado;
	}

	public boolean isFlagGuardarMemoria() {
		return flagGuardarMemoria;
	}

	public void setFlagGuardarMemoria(boolean flagGuardarMemoria) {
		Calculadora.flagGuardarMemoria = flagGuardarMemoria;
	}

	public boolean isFlagUsarMemoria() {
		return flagUsarMemoria;
	}

	public void setFlagUsarMemoria(boolean flagUsarMemoria) {
		Calculadora.flagUsarMemoria = flagUsarMemoria;
	}

	public String[] getNumerosEnMemoria() {
		return numerosEnMemoria;
	}

	public void setNumerosEnMemoria(String[] numerosEnMemoria) {
		Calculadora.numerosEnMemoria = numerosEnMemoria;
	}
	
	public String escribirNumero(String n, char c){		
		return Funciones.escribirNumero(this,n, c);
	}

	public String guardarNum_Operacion(String s, char c) {
		if(!s.equals("")) {
			numero=Funciones.guardarNumero(s);
			operacion=c;
			return "";
		}
			return "";
	}

	public String mostrarResultado(String s) {
		String aux =Funciones.resolver(operacion,numero,s); 
		numero=Funciones.guardarNumero(s);
		flagResultado=false;
		operacion=' ';
		return aux;
	}

	public String mostrarCuenta(String s) {
		return Funciones.mostrarCuenta(numero,operacion,s);
	}

	public void limpiarTodo() {
		numero="0";
		flagResultado=false;
		flagGuardarMemoria=false;
		flagUsarMemoria=false;
		numerosEnMemoria=new String[10];
	}
	
	public void limpiarMemoria(){
		numerosEnMemoria=new String[10];
	}

	public void hablar(String oracion) {
		VoiceManager manager= VoiceManager.getInstance();
		Voice voz = manager.getVoice("kevin");
		voz.allocate();
		voz.speak(oracion);
		voz.deallocate();
	}
}
