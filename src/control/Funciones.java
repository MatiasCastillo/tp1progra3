package control;

import java.math.BigDecimal;

public class Funciones {
	static BigDecimal x;
	static BigDecimal y;

	public static String resolver(char c,String i,String j){
		if(c==' '){
			return i;
		}
		if(c=='-'){
			return resta(i,j);
		}
		else if(c=='+'){
			return suma(i,j);
		}
		else if(c=='*'){
			return multiplicacion(i,j);
		}
		else if(c=='/'){
			return division(i,j);
		}
		return "Error";
	}

	public static String suma (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.add(y);
		return String.valueOf(resultado);
		
	}
	
	public static String resta (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.subtract(y);
		return String.valueOf(resultado);
	}
	
	public static String multiplicacion (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.multiply(y);
		return String.valueOf(resultado);
	}
	
	public static String division (String i , String j){
				
		if(y.equals("0")) {
			return "Imposible dividir por cero";
		}
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.divide(y);
		return String.valueOf(resultado);
	}
	
	public static String guardarNumero(String s){
		if(s!=""){
			return s;
		}
		else{
			return "Ingrese un numero";
		}
	}
	
	public static String borrarUnDigito(String s){
		String str=s;
		if (str != "" && str.length() > 0 ) {
		      str = str.substring(0, str.length()-1);
		    }
		    return str;
	}
	
	public static String concatenar(String s, char c){
			return s+c;
	}
	
	public static String decimal(String s1){
		String s=s1;
		boolean entero=true;
		for(int i=0; i<s.length();i++){
			if(s.charAt(i)=='.'){
				entero= false;
			}
		}
		if(entero){
			s=s+'.';
		}
		return s;
	}
	
	public static String mostrarCuenta(String s1,char o,String s2){
		return s1+o+s2;
	}
	
	public static String deshacer(String s){
		String ret="";
		boolean check=true;
		if(!s.equals(""))
			for(int i=0; i<s.length();i++){
				if(check){
					if(s.charAt(i)=='+'||s.charAt(i)=='-'||s.charAt(i)=='*'||s.charAt(i)=='/'){
						check=false;
					}
					else{
						ret=ret+s.charAt(i);
					}
				}
			}
		return ret;
	}
	
	public static String escribirNumero(Calculadora cal, String n,char c){
		String[] lista= cal.getNumerosEnMemoria();
	
		if(cal.isFlagGuardarMemoria()){
			lista[Character.getNumericValue(c)]=n;
			cal.setNumerosEnMemoria(lista);
			cal.setFlagGuardarMemoria(false);
			return "";
		}		
		else if(cal.isFlagUsarMemoria()){
			cal.setFlagUsarMemoria(false);
			return cal.getNumerosEnMemoria()[Character.getNumericValue(c)];
		}
		else{
			if(cal.isFlagResultado()){	
					return Funciones.concatenar(n,c);
				}else{
					n="";
					cal.setFlagResultado(true);
					return Funciones.concatenar(n,c);
				}
			}
		}
}
